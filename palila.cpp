///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author @Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @27_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include "palila.hpp"

using namespace std;

namespace animalfarm{

   Palila::Palila ( string newwhereFound, enum Color newColor, enum Gender newGender ){
      whereFound = newwhereFound;
      gender = newGender;
      species = "Loxioides bailleui";
      featherColor = newColor;
      isMigratory = false;
   }

   void Palila::printInfo(){
      cout << "Palila" << endl;
      cout <<" Where Found = [" << whereFound << "]" << endl;
         Bird::printInfo();
   }
}
