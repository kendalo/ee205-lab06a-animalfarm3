///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @27_Feb_2021
///////////////////////////////////////////////////////////////////////////////
#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "aku.hpp"
using namespace std;
namespace animalfarm{
Animal* AnimalFactory::getRandomAnimal() {
Animal* newAnimal = NULL;
int i = rand()%6; // This gives you a number from 0 to 5
switch (i) {
case 0: newAnimal = new Cat ( Cat::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
case 1: newAnimal = new Dog ( Dog::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
case 2: newAnimal = new Nunu ( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
case 3: newAnimal = new Aku ( Animal::getRandomWeight(1.0, 12.5), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
case 4: newAnimal = new Palila( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
case 5: newAnimal = new Nene ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
        break;
return newAnimal;
}
return newAnimal;
}
}
